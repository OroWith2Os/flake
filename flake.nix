{
  description = "Nix flake for Oro's system(s)";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    home-manager = {
      url = "github:nix-community/home-manager/master";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nix-flatpak.url = "github:gmodena/nix-flatpak/?ref=latest";
  };

  outputs = {
    self,
    nixpkgs,
    home-manager,
    nix-flatpak,
    ...
  } @ inputs: {
    nixosConfigurations.probook = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      modules = [
        ./devices/probook/base.nix
        home-manager.nixosModules.home-manager
        {
          home-manager = {
            useGlobalPkgs = true;
            useUserPackages = true;
            users.oro.imports = [
              ./users/oro/home.nix
              nix-flatpak.homeManagerModules.nix-flatpak
              ./users/oro/flatpak.nix
            ];
          };
        }
      ];
    };
    formatter.x86_64-linux = nixpkgs.legacyPackages.x86_64-linux.alejandra;
  };
}
