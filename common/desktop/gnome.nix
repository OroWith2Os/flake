{
  pkgs,
  config,
  ...
}: {
  imports = [
    ../audio.nix
    ../core.nix
  ];

  services.xserver = {
    displayManager.gdm.enable = true;
    desktopManager.gnome.enable = true;
    xkb = {
      layout = "us";
      variant = "";
    };
  };
  services.gnome.core-utilities.enable = false;
  # We disabled all the core packages above. Let's add back what we want.
  environment.systemPackages = with pkgs;
    [
      ptyxis
      gnome-disk-utility
      nautilus
    ]
    ++ lib.optionals config.services.flatpak.enable [
      # Same reasoning as upstream nixpkgs - only enable gnome-software if flatpak is enabled.
      gnome-software
    ];
  nixpkgs.overlays = [
    (final: prev: {
      ptyxis = prev.ptyxis.overrideAttrs (oldAttrs: {
        # This way it shows up in the UI as Terminal, instead of Ptyxis.
        mesonFlags = (oldAttrs.mesonFlags or []) ++ ["-Dgeneric=terminal"];
      });
    })
  ];
}
