{
  lib,
  pkgs,
  ...
}: {
  nixpkgs.config.allowUnfree = true;
  nix = {
    settings = {
      auto-optimise-store = true;
      experimental-features = ["nix-command" "flakes"];
    };
    gc = {
      automatic = true;
      dates = "17:00";
      persistent = true;
      options = "--delete-older-than 30d";
    };
    package = lib.mkDefault pkgs.nixVersions.stable;
  };
}
