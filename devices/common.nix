{pkgs, ...}: {
  imports = [
    ../common/nix.nix
  ];

  environment.variables.EDITOR = "nvim";

  programs = {
    neovim.enable = true;
    git.enable = true;
  };

  environment.systemPackages = with pkgs; [
    wget
  ];
}
