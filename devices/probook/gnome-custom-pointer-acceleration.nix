{pkgs, ...}: {
  nixpkgs.overlays = [
    (final: prev: {
      gsettings-desktop-schemas = prev.gsettings-desktop-schemas.overrideAttrs (oldAttrs: {
        patches =
          (oldAttrs.patches or [])
          ++ [
            (pkgs.fetchpatch {
              url = "https://gitlab.gnome.org/GNOME/gsettings-desktop-schemas/-/merge_requests/99/diffs.patch?diff_id=1344271";
              sha256 = "3sk6lSWHpmrwWYWROcYEquitU2cwylJsEHnhwwtYk+Q=";
            })
          ];
      });
      mutter = prev.mutter.overrideAttrs (oldAttrs: {
        patches =
          (oldAttrs.patches or [])
          ++ [
            (pkgs.fetchpatch {
              url = "https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/4296/diffs.patch?diff_id=1344087";
              sha256 = "XD5FOcT99yoc4Svrm2z1RxyccMAkPJOAHBltuLUyWOs=";
            })
          ];
      });
    })
  ];
}
