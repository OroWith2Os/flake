{pkgs, ...}: {
  imports = [
    ../common.nix
    ./filesystems.nix
    ./hardware-configuration.nix
    ../../users/oro/base.nix
    ./gnome-custom-pointer-acceleration.nix
    ../../common/desktop/gnome.nix
    ../../common/podman.nix
  ];

  time.timeZone = "America/Chicago";
  i18n.defaultLocale = "en_US.UTF-8";

  networking.hostName = "kirara2";

  services.flatpak.enable = true;
  services.fprintd.enable = true;

  security.sudo.wheelNeedsPassword = false;

  boot = {
    tmp.useTmpfs = true;
    plymouth = {
      enable = true;
      theme = "bgrt";
    };
  };

  environment.systemPackages = with pkgs; [
    powertop
  ];

  system.stateVersion = "24.11";
}
