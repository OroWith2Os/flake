_: {
  services = {
    flatpak = {
      remotes = [
        {
          name = "flathub";
          location = "https://dl.flathub.org/repo/flathub.flatpakrepo";
        }
        {
          name = "flathub-beta";
          location = "https://flathub.org/beta-repo/flathub-beta.flatpakrepo";
        }
        {
          name = "gnome-nightly";
          location = "https://nightly.gnome.org/gnome-nightly.flatpakrepo";
        }
      ];

      packages = [
        # Flathub, not organized.
        {
          appId = "org.mozilla.firefox";
          origin = "flathub";
        }
        {
          appId = "com.discordapp.Discord";
          origin = "flathub";
        }
        {
          appId = "io.bassi.Amberol";
          origin = "flathub";
        }
        {
          appId = "com.github.tchx84.Flatseal";
          origin = "flathub";
        }
        {
          appId = "com.obsproject.Studio";
          origin = "flathub";
        }
        {
          appId = "re.sonny.Commit";
          origin = "flathub";
        }
        {
          appId = "md.obsidian.Obsidian";
          origin = "flathub";
        }

        # Flathub, GNOME
        {
          appId = "org.gnome.TextEditor";
          origin = "flathub";
        }
        {
          appId = "org.gnome.Loupe";
          origin = "flathub";
        }
        {
          appId = "org.gnome.Calculator";
          origin = "flathub";
        }
        {
          appId = "ca.desrt.dconf-editor";
          origin = "flathub";
        }
        {
          appId = "re.sonny.Workbench";
          origin = "flathub";
        }
        {
          appId = "org.gnome.Calendar";
          origin = "flathub";
        }
        {
          appId = "org.gnome.Showtime";
          origin = "flathub";
        }

        # GNOME Nightly
        {
          appId = "org.gnome.Builder.Devel";
          origin = "gnome-nightly";
        }
        {
          appId = "org.gnome.Fractal.Devel";
          origin = "gnome-nightly";
        }
      ];

      overrides = {
        # Note to self: don't set "!x11", otherwise Discord crashes on startup. The client will use Wayland just fine with only this.
        "com.discordapp.Discord".Context.sockets = ["wayland"];
        "md.obsidian.Obsidian".Context.sockets = ["wayland"];
      };
    };
  };
}
