{
  config,
  pkgs,
  ...
}: {
  home = {
    username = "oro";
    homeDirectory = "/home/oro";
    stateVersion = "25.05";
  };
  programs.home-manager.enable = true;

  imports = [
    ./fish.nix
    ./gnome.nix
  ];

  programs.git = {
    enable = true;
    userName = "Dallas Strouse";
    userEmail = "dastrouses@gmail.com";
    signing = {
      # https://docs.github.com/en/authentication/managing-commit-signature-verification/telling-git-about-your-signing-key
      key = "2E317653460259B3";
      signByDefault = true;
      format = "openpgp";
    };
    extraConfig = {
      safe.directory = "*";
      init.defaultBranch = "main";
      core.editor = "flatpak run re.sonny.Commit";
    };
  };
}
