{pkgs, ...}: {
  # We want home-manager to manage fish, but can't set it as the default shell
  # here without enabling it again like this. There might be a workaround, I
  # don't know. But this works for now.
  programs.fish.enable = true;

  users.users.oro = {
    isNormalUser = true;
    home = "/home/oro";
    description = "Dallas Strouse";
    extraGroups = ["networkmanager" "wheel"];
    shell = pkgs.fish;
    # Maps to net.nokyan.Resources on Flathub. Can't use the Flatpak because
    # the binary Resources ships on Flathub isn't statically linked, and it's
    # spawned on the host.
    packages = with pkgs; [resources distrobox gnomeExtensions.blur-my-shell gnomeExtensions.caffeine];
  };
}
