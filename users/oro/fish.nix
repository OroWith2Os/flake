_: {
  programs.fish = {
    enable = true;

    functions = {
      fish_greeting = "";
    };
  };

  programs.starship.enable = true;
}
