{
  lib,
  pkgs,
  ...
}: {
  dconf = {
    enable = true;
    settings = {
      "org/gnome/mutter" = {
        experimental-features = ["scale-monitor-framebuffer" "kms-modifiers" "autoclose-xwayland" "xwayland-native-scaling"];
        dynamic-workspaces = true;
        workspaces-only-on-primary = true;
        edge-tiling = true;
      };
      "org/gnome/desktop/interface" = {
        show-battery-percentage = true;
        clock-show-weekday = true;
      };
      "org/gnome/desktop/background" = {
        picture-uri = "file:///run/current-system/sw/share/backgrounds/gnome/adwaita-l.jxl";
        picture-uri-dark = "file:///run/current-system/sw/share/backgrounds/gnome/adwaita-d.jxl";
        # Change these colors in sync with the background above
        primary-color = "#3071AE";
        secondary-color = "#000000";
      };
      "org/gnome/desktop/screensaver" = {
        # Tracks org/gnome/desktop/background/{picture-uri, primary-color, secondary-color}
        picture-uri = "file:///run/current-system/sw/share/backgrounds/gnome/adwaita-l.jxl";
        primary-color = "#3071AE";
        secondary-color = "#000000";
      };
      "org/gnome/shell" = {
        favorite-apps = [
          "org.mozilla.firefox.desktop"
          "org.gnome.Ptyxis.desktop"
          "com.discordapp.Discord.desktop"
          "org.gnome.Nautilus.desktop"
          "org.gnome.Software.desktop"
          "org.gnome.Builder.Devel.desktop"
        ];
        enabled-extensions = with pkgs.gnomeExtensions; [
          blur-my-shell.extensionUuid
          caffeine.extensionUuid
        ];
      };
      "org/gnome/desktop/search-providers".enabled = ["org.gnome.Calculator.desktop" "org.gnome.Calendar.Devel.desktop"];
      # Ten minutes to screen shutoff. See Settings -> Power -> Screen Blank.
      "org/gnome/desktop/session".idle-delay = lib.hm.gvariant.mkUint32 600;
      # Just get hearing aids, you bitch
      "org/gnome/desktop/sound".allow-volume-above-100-percent = true;
      "org/gnome/desktop/wm/keybindings".toggle-fullscreen = ["F11"];

      "org/gnome/system/location".enabled = true;
      "org/gnome/settings-daemon/plugins/power".sleep-inactive-ac-type = "nothing";

      "org/gnome/shell/extensions/caffeine" = {
        restore-state = true;
        show-indicator = "always";
      };

      # NOTE: these don't need the mkInt32 hacks. If they break, though, try `lib.hm.gvariant.MkInt32`
      # Default hacks level. Performance has artifacting with app folders.
      "org/gnome/shell/extensions/blur-my-shell".hacks-level = 1;
      # Light style on folders in the app drawer
      "org/gnome/shell/extensions/blur-my-shell/appfolder".style-dialogs = 2;
      # Makes sure the panel doesn't become plain-white all around when prefer-light is set
      "org/gnome/shell/extensions/blur-my-shell/panel".override-background = false;
    };
  };
}
